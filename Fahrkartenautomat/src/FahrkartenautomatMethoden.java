import java.util.Scanner;

class FahrkartenautomatMethoden {
	public static void main(String[] args) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rueckgabebetrag);
	}
	
	public static double fahrkartenbestellungErfassen() {
		double Auswahl;
	    double Einzelfahrschein = 2.90;
	    double Tageskarte = 8.60;
	    double Kleingruppen = 23.50;
	    Scanner tastatur = new Scanner(System.in);
	    System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:  ");
	    System.out.println("Einzelfahrschein Regeltarif AB [2,90] (1)");
	    System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
	    System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
	    Auswahl = tastatur.nextDouble();
	    double zuZahlenderBetrag = 0;
	    if (Auswahl == 1) {
	    	 zuZahlenderBetrag += Einzelfahrschein;
	    }
	    else if (Auswahl == 2) {
	        zuZahlenderBetrag += Tageskarte;
	    }
	    else if (Auswahl == 3) {
	        zuZahlenderBetrag += Kleingruppen;
	    }
	    else {
	        System.out.println("Falsche Eingabe");
	        // Wir wissen nicht wie wir die Schleife neu starten, vllt können Sie uns da weiterhelfen
	    }
	    int ticketAnzahl = 0;
	    System.out.println("\nWieviele Tickets benötigen Sie?");
	    ticketAnzahl = tastatur.nextInt();
	    System.out.print("\nAnzahl der Tickets: " + ticketAnzahl);
	    zuZahlenderBetrag *= ticketAnzahl;
	    return zuZahlenderBetrag;
	    }

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double rueckgabebetrag;
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rueckgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		if (rueckgabebetrag > 0.00) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro\n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.00) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.00;
			}
			while (rueckgabebetrag >= 1.00) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.00;
			}
			while (rueckgabebetrag >= 0.50) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.50;
			}
			while (rueckgabebetrag >= 0.20) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.20;
			}
			while (rueckgabebetrag >= 0.10) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.10;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

}